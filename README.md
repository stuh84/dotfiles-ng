# My dotfiles

- Uses Ansible
- Linting and syntax checking with Drone

## OS Support

Levels of support: -

- Primary - Tested on, works with on new or existing machines, all roles that are applicable will install
- Secondary - Should work with new machines, may require some manual intervention on existing machines, not actively tested on, some roles may not work
- Best Efforts - No testing, fixes for these operating systems will not be prioritised, not tested on
- No support - Will not work at all with this

|Operating System|Primary Support|Secondary Support|Best Efforts|No support|Notes|
|--|--|--|--|--|--|
|MacOS Arm|☑️|||||
|MacOS Intel||☑️|||Tested on KVM-based MacOS VM|
|Debian amd64|☑️|||||
|Ubuntu amd64|☑️||||Also works on Pop!_OS|
|Ubuntu arm64||☑️|||Most roles work, but anything that installs flatpaks or debs directly is flakey, as not everything is available|
|Alpine||||☑️|No longer supported - previous approach can be found in old commits|
|RHEL-based||☑️||||
|Suse-based|||☑️||Infrequently tested|
|Othe  Linux distributions|||☑️|||
|Windows||||☑️||

## Included roles/features

- Install of MacOS Command Line Tools, using `elliotweiser.osx-command-line-tools`
- Base set of packages
- Install Flatpak, and Flathub remote on Linux (particularly on Ubuntu and derivatives)
- Install `zsh`, configure with `oh-my-zsh`, and customise to my preferences
- Install `nodejs` and `npm`, for certain LSP packages
- Install `rust`, and certain `cargo` packages
- Install and configure `neovim` using [kickstart.nvim](https://github.com/nvim-lua/kickstart.nvim) as a base, with much customisation
  - Also has support for configuring `vim`, but I don't use `vim` unless `neovim` is unavailable
- Install `starship` and add `starship` configuration for my shell prompt on my servers/VPSs
- Install `wezterm` and apply configuration
- Configure `tmux`
- Install `nerdfonts` (by default, installs a subset, but can install all `nerdfonts`)
- Install a list of fonts, provided by URL
- Configure `bash` prompt
- Install Hashicorp packages
- Install OpenTofu
- Install `kubectl`, `kubectx`, `k9s`, `kubectx` and `kubens`
- Install Helm version 3, and the `helm diff` plugin
- Install some desktop packages (mainly editors)
- Install a number of packages for my personal day-to-day use (e.g. music, recording, entertainment, browsers, file storage)
- Configure `git` the way I like it
  - Sets my name and email
  - Don't rebase on `git pull`
  - Auto create remote branch on push if it doesn't exist
  - Set default branch to `main`
- Install `dev` packages
  - Currently this installs `sdkman` for managing different Java runtimes
- Install `admin` packages
  - `gotify` client
  - RDP/VNC clients
  - `virt-manager`
- Install `incus`
- Install `distrobox`
- Configure different desktop environments (mainly setting terminal defaults)
  - `dconf` for many GTK-based environments (Gnome, MATE, Cinnamon)
  - `gnome` role for installing Gnome extensions
  - `kde` role for KDE
  - `xfce` role for XFCE
- Setting MacOS defaults and remove unneeded items from the dock on first run
- Installing Debian-specific packages (from Apt/Debs)
  - `syncthing`
  - `docker`
  - Arbitrary `.deb` installs

## Inspiration

Inspiration for this repository comes from a few different places: -

- A heavily modified version of [this](https://github.com/gantsign/ansible-role-oh-my-zsh) role for `oh-my-zsh`
- Jeff Geerling's [mac-dev-playbook](https://github.com/geerlingguy/mac-dev-playbook) for a few of the MacOS-specific approaches
- Waylon Walker's [guide](https://waylonwalker.com/install-rust/) on installing Rust/Cargo with Ansible

There are probably others, but this has been in the works for a few years now so I may have forgotten some!

## Personal Mac run

```shell
ansible-galaxy install -r requirements.yml
ansible-playbook --connection=local --inventory='127.0.0.1,' --limit='127.0.0.1' -e 'p10k=true' -e 'is_desktop=true' -e 'is_personal=true' -e 'is_nerdfonts_lite=true' -e 'is_fonts=true' --ask-become-pass playbook.yml
```

- Include `-e 'is_clean_dock=true'` to remove initial apps from dock, should rarely/ever be needed after first set up
- Include `-e 'is_nerdfonts=true'` to install nerdfonts, rarely need to run this after initial install 
  - Use instead of `is_nerdfonts_lite=true` so that all NerdFonts are installed
- Include `-e 'is_admin=true'` to install tools like RDP clients, virtual machine management
- `-e 'is_incus=true'` - Installs Incus client 

## Work Mac run

```shell
ansible-galaxy install -r requirements.yml
ansible-playbook --connection=local --inventory='127.0.0.1,' --limit='127.0.0.1' -e 'p10k=true' -e 'is_desktop=true' -e 'is_dev=true' -e 'is_nerdfonts_lite=true' -e 'is_fonts=true' --ask-become-pass playbook.yml
```
- Include `-e 'is_nerdfonts=true'` to install nerdfonts, rarely need to run this after initial install 
  - Use instead of `is_nerdfonts_lite=true` so that all NerdFonts are installed
- Include `-e 'is_admin=true'` to install tools like RDP clients, virtual machine management

## Personal Linux run 

```shell
ansible-galaxy install -r requirements.yml
ansible-playbook --connection=local --inventory='127.0.0.1,' --limit='127.0.0.1' -e 'p10k=true' -e 'is_desktop=true' -e 'is_personal=true' -e 'root_group=root' -e 'is_flatpaks=true' -e 'is_nerdfonts_lite=true' -e 'is_distrobox=true' -e 'is_fonts=true' --ask-become-pass playbook.yml
```

- `-e 'is_docker=true'` - Installs Docker and associated packages
- `-e 'is_incus=true'` - Installs Incus and associated packages
- `-e 'is_syncthing=true'` - Installs Syncthing 
- Include `-e 'is_nerdfonts=true'` to install nerdfonts, rarely need to run this after initial install 
  - Use instead of `is_nerdfonts_lite=true` so that all NerdFonts are installed
- Include `-e 'is_admin=true'` to install tools like RDP clients, virtual machine management
- Include `-e 'is_distrobox=true'` to install Distrobox tools

## Linux Server run

### Local

```shell
ansible-galaxy install -r requirements.yml
ansible-playbook --connection=local --inventory='127.0.0.1,' --limit='127.0.0.1' -e 'is_server=true' -e 'root_group=root' --ask-become-pass playbook.yml
```

### Remote

```shell
ansible-galaxy install -r requirements.yml
ansible-playbook --flush-cache -e 'ansible_user=stuh84' -e 'is_server=true' -e 'root_group=root' --ask-become-pass playbook.yml
```

The above is used to ensure that we aren't using cached inventory variables, which may happen when sharing inventory with other machines.
