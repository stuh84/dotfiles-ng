---
- name: Add Sublime Text Apt GPG key
  ansible.builtin.get_url:
    url: https://download.sublimetext.com/sublimehq-pub.gpg
    dest: /etc/apt/keyrings/sublime.asc
    mode: "0644"
    force: true
  when:
    - ansible_os_family == 'Debian'
    - is_desktop is defined

- name: Add Sublime Text Apt Repository
  ansible.builtin.apt_repository:
    repo: deb [signed-by=/etc/apt/keyrings/sublime.asc] https://download.sublimetext.com/ apt/stable/
    state: present
    filename: sublime
  when:
    - ansible_os_family == 'Debian'
    - is_desktop is defined

- name: Add Sublime Text RPM repository - RPM
  ansible.builtin.yum_repository:
    name: sublime
    description: Sublime Text RPM Packages
    baseurl: https://download.sublimetext.com/rpm/stable/x86_64
    gpgkey: https://download.sublimetext.com/sublimehq-rpm-pub.gpg
    enabled: true
    gpgcheck: true
  when:
    - ansible_os_family == 'RedHat' or ansible_os_family == 'Rocky'
    - is_desktop is defined
    - ansible_architecture == 'x86_64'

- name: Install Editor packages for Debian-like OS
  ansible.builtin.apt:
    name: "{{ global.desktop.editors.packages | difference(desktop.editors.packages.exclude|default([])) | union(desktop.editors.packages.include|default([])) }}"
    update_cache: true
    state: latest
  when:
    - ansible_os_family == 'Debian'
    - is_desktop is defined
    - global.desktop.editors.packages | difference(desktop.editors.packages.exclude|default([])) | union(desktop.editors.packages.include|default([])) | length >
      0

- name: Install Editor packages for Red Hat-like OS
  ansible.builtin.dnf:
    name: "{{ global.desktop.editors.packages | difference(desktop.editors.packages.exclude|default([])) | union(desktop.editors.packages.include|default([])) }}"
    update_cache: true
    state: latest
  when:
    - ansible_os_family == 'RedHat' or ansible_os_family == 'Rocky'
    - is_desktop is defined
    - ansible_architecture == 'x86_64'
    - global.desktop.editors.packages | difference(desktop.editors.packages.exclude|default([])) | union(desktop.editors.packages.include|default([])) | length >
      0

- name: Install Editor packages - MacOS
  become: true
  become_user: "{{ user_username }}"
  community.general.homebrew:
    name: "{{ global.desktop.editors.packages | difference(desktop.editors.packages.exclude|default([])) | union(desktop.editors.packages.include|default([])) }}"
    state: latest
  when:
    - ansible_os_family == 'Darwin'
    - is_desktop is defined
    - global.desktop.editors.packages | difference(desktop.editors.packages.exclude|default([])) | union(desktop.editors.packages.include|default([])) | length >
      0

- name: Install Editor packages - MacOS Casks
  become: true
  become_user: "{{ user_username }}"
  community.general.homebrew_cask:
    name: "{{ global.desktop.editors.casks.packages | difference(desktop.editors.casks.packages.exclude|default([])) | union(desktop.editors.casks.packages.include|default([]))
      }}"
    state: latest
  when:
    - ansible_os_family == 'Darwin'
    - is_desktop is defined
    - global.desktop.editors.casks.packages | difference(desktop.editors.casks.packages.exclude|default([])) | union(desktop.editors.casks.packages.include|default([]))
      | length > 0

- name: Install Editor packages - Flatpaks
  become: true
  become_user: "{{ user_username }}"
  community.general.flatpak:
    name: "{{ global.desktop.editors.flatpaks | difference(desktop.editors.flatpaks.exclude|default([])) | union(desktop.editors.flatpaks.include|default([])) }}"
    state: present
    method: user
  when:
    - is_desktop is defined
    - is_flatpaks is defined
    - (global.desktop.editors.flatpaks | difference(desktop.editors.flatpaks.exclude|default([])) | union(desktop.editors.flatpaks.include|default([])) | length >
      0)
