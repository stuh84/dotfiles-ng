local status, bufferline = pcall(require, "bufferline")
if (not status) then return end

bufferline.setup({
    options = {
        --      mode = "tabs",
        separator_style = 'thin',
        always_show_bufferline = false,
        show_buffer_close_icons = true,
        show_close_icon = true,
        color_icons = true,
        diagnostics = "nvim_lsp"
    },
    -- highlights = {
    --     separator_selected = {
    --         bg = '#282a36',
    --         fg = '#282a36'
    --     },
    -- }
})

vim.keymap.set('n', '<C-Down>', '<Cmd>BufferLineCycleNext<CR>', {})
vim.keymap.set('n', '<C-Up>', '<Cmd>BufferLineCyclePrev<CR>', {})
vim.cmd [[
    autocmd!
    autocmd ColorScheme * highlight BufferLineFill guibg=#282a36
    autocmd ColorScheme * highlight BufferLineSeparator guifg=#282a36
    autocmd ColorScheme * highlight BufferLineSeparatorSelected guifg=#282a36
]]
