local status, colorizer = pcall(require, "colorizer")
if (not status) then return end

if vim.env.TERM_PROGRAM ~= "Apple_Terminal" then
    colorizer.setup({
        '*';
    })
end
