local status, gitsigns = pcall(require, "gitsigns")
if (not status) then return end

gitsigns.setup {
    signs = {
        add          = { text = '+' },
        change       = { text = '~' },
        delete       = { text = '_' },
        topdelete    = { text = '‾' },
        changedelete = { text = '≃' },
    },
    numhl = true,
    linehl = false,
    word_diff = true,
    current_line_blame = true,
}
