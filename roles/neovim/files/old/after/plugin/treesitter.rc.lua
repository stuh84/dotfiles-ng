local status, ts = pcall(require, "nvim-treesitter.configs")
if (not status) then return end

ts.setup {
    highlight = {
        enable = true,
        disable = {},
    },
    indent = {
        enable = true,
        disable = {
            "python",
        },
    },
    ensure_installed = {
        "tsx",
        "toml",
        "php",
        "json",
        "yaml",
        "swift",
        "css",
        "html",
        "lua",
        "go",
        "python",
        "java",
        "make",
        "markdown",
        "markdown_inline",
        "rust",
        "bash",
        "json",
        "typescript",
        "javascript",
        "hcl",
        "terraform",
    },
    autotag = {
        enable = true,
    },
}

local parser_config = require 'nvim-treesitter.parsers'.get_parser_configs()
parser_config.gotmpl = {
    install_info = {
        url = "https://github.com/ngalaiko/tree-sitter-go-template",
        files = { "src/parser.c" }
    },
    filetype = "gotmpl",
    used_by = { "gohtmltmpl", "gotexttmpl", "gotmpl", "yaml" }
}
