require('stuh84.base')
require('stuh84.plugins')
require('stuh84.highlights')
require('stuh84.keymaps')

if vim.env.TERM_PROGRAM == "Apple_Terminal" then
    vim.opt.termguicolors = false
else
    vim.opt.termguicolors = true
    vim.cmd [[colorscheme dracula]]
end
