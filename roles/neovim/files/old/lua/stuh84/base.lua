vim.cmd [[
    autocmd!
    autocmd ColorScheme * highlight BufferLineFill guibg=#282a36
    autocmd ColorScheme * highlight BufferLineSeparator guifg=#282a36
    autocmd ColorScheme * highlight BufferLineSeparatorSelected guifg=#282a36
]]

vim.scriptencoding = 'utf-8'
vim.opt.encoding = 'utf-8'
vim.opt.fileencoding = 'utf-8'

vim.wo.number = false

vim.opt.title = true
vim.opt.autoindent = true
vim.opt.smartindent = true
vim.opt.smarttab = true

vim.opt.shell = 'zsh'

vim.bo.expandtab = true
vim.bo.shiftwidth = 4
vim.bo.tabstop = 4
vim.bo.softtabstop = 4


vim.opt.backspace = { 'start', 'eol', 'indent' }
