vim.keymap.set(
    'n',
    '<F4>',
    function()
        vim.wo.number = not vim.wo.number
    end
) -- Toggle number

vim.keymap.set(
    'n',
    '<F7>',
    function()
        vim.o.relativenumber = not vim.o.relativenumber
        vim.o.number = not vim.o.number
    end
) -- Toggle number

vim.keymap.set(
    'n',
    '<F8>',
    function()
        vim.cmd('split')
        vim.cmd('wincmd w')
    end
)

vim.keymap.set(
    'n',
    '<F9>',
    function()
        vim.cmd('vsplit')
        vim.cmd('wincmd w')
    end
)

vim.keymap.set(
    'n',
    '<F10>',
    function()
        for i = 1, #vim.api.nvim_list_wins(), 1 do
            vim.cmd('wincmd w')
            vim.cmd('wincmd =')
        end
    end
)

vim.keymap.set('n', 'Y', 'yy') -- Keep old Yank behaviour

vim.keymap.set(
    'n',
    'mp',
    ':MarkdownPreview<CR>',
    { silent = true }
)

vim.keymap.set(
    'n',
    'cr',
    ':FloatermNew! cargo run<CR>'
)

vim.keymap.set(
    'n',
    'cd',
    function()
        vim.diagnostic.disable()
    end
)

function map(mode, lhs, rhs, opts)
    local options = { noremap = true }
    if opts then
        options = vim.tbl_extend("force", options, opts)
    end
    vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end


map('n', "<leader>ft", ":FloatermNew --name=myfloat --height=0.8 --width=0.7 --autoclose=2 fish <CR> ")
map('n', "t", ":FloatermToggle myfloat<CR>")
map('t', "<Esc>", "<C-\\><C-n>:q<CR>")
