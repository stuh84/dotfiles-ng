local status, packer = pcall(require, "packer")
if (not status) then
    print("Packer is not installed")
    return
end

vim.cmd [[packadd packer.nvim]]

local ensure_packer = function()
    local fn = vim.fn
    local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
    if fn.empty(fn.glob(install_path)) > 0 then
        fn.system({ 'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path })
        vim.cmd [[packadd packer.nvim]]
        return true
    end
    return false
end

local packer_bootstrap = ensure_packer()

packer.startup(function(use)
    use 'wbthomason/packer.nvim'
    use 'Mofiqul/dracula.nvim'
    use 'onsails/lspkind-nvim'
    use 'hrsh7th/cmp-buffer'   -- nvim-cmp source for buffer words
    use 'hrsh7th/cmp-nvim-lsp' -- nvim-cmp source for neovim's built-in LSP
    use 'hrsh7th/nvim-cmp'     -- Completion
    use 'jose-elias-alvarez/null-ls.nvim'
    use 'nvim-tree/nvim-web-devicons'
    use {
        'nvim-lualine/lualine.nvim',
        requires = { 'nvim-tree/nvim-web-devicons', opt = true }
    }
    use 'neovim/nvim-lspconfig'
    use 'williamboman/mason.nvim'

    use 'williamboman/mason-lspconfig.nvim'


    use({
        'nvimdev/lspsaga.nvim', -- LSP UIs
        requires = {
            { "nvim-tree/nvim-web-devicons" },
            { "nvim-treesitter/nvim-treesitter" }
        }
    })
    use 'L3MON4D3/LuaSnip'

    use 'lewis6991/gitsigns.nvim'
    use 'dinhhuy258/git.nvim' -- For git blame & browse

    use {
        'nvim-treesitter/nvim-treesitter',
        run = function() require('nvim-treesitter.install').update({ with_sync = true }) end,
    }
    use 'norcalli/nvim-colorizer.lua'
    use 'akinsho/nvim-bufferline.lua'
    use({
        "iamcco/markdown-preview.nvim",
        run = function() vim.fn["mkdp#util#install"]() end,
    })
    use {
        'nvim-telescope/telescope.nvim',
        requires = { { 'nvim-lua/plenary.nvim' } }
    }
    use 'nvim-telescope/telescope-file-browser.nvim'
    use 'nvim-treesitter/nvim-treesitter-context'
    if packer_bootstrap then
        require('packer').sync()
    end

    use {
        "windwp/nvim-autopairs",
        config = function() require("nvim-autopairs").setup {} end
    }
    use {
        'numToStr/Comment.nvim',
        config = function()
            require('Comment').setup()
        end
    }

    use 'folke/tokyonight.nvim'

    use 'RRethy/vim-illuminate'

    -- use 'simrat39/rust-tools.nvim'
    -- use 'kdarkhan/rust-tools.nvim'
    use {
        "mrcjkb/rustaceanvim",
        version = '^3',
        ft = { 'rust' },
    }
    use 'mfussenegger/nvim-dap'

    use 'voldikss/vim-floaterm'

    use 'danilamihailov/beacon.nvim'

    use 'echasnovski/mini.nvim'

    use {
        "ThePrimeagen/refactoring.nvim",
        requires = {
            { "nvim-lua/plenary.nvim" },
            { "nvim-treesitter/nvim-treesitter" }
        }
    }
    use "lukas-reineke/lsp-format.nvim"
    use {
        "folke/todo-comments.nvim",
        requires = {
            { "nvim-lua/plenary.nvim" }
        }
    }
    use "onsails/lspkind-nvim"
end)
