local status, saga = pcall(require, "lspsaga")
if (not status) then return end


saga.setup {
}

local opts = { noremap = true, silent = true }
vim.keymap.set('n', 'K', '<Cmd>Lspsaga hover_doc<CR>', opts)
